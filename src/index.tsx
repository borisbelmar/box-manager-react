import React from 'react'
import ReactDOM from 'react-dom'
import Axios from 'axios'
import App from './App'
import * as serviceWorker from './serviceWorker'
import './assets/styles.css'
import 'react-toastify/dist/ReactToastify.css'

Axios.defaults.baseURL = process.env.REACT_APP_BOX_MANAGER_API || 'http://localhost:6002'

ReactDOM.render(
  <App />,
  document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
