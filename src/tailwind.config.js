/* eslint-disable global-require */
module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [
    './src/**/*.tsx'
  ],
  theme: {
    extend: {
      colors: {
        primary: {
          100: '#d4ebf2',
          200: '#a9d7e5',
          300: '#7dc2d8',
          400: '#52aecb',
          500: '#279abe',
          600: '#1f7b98',
          700: '#175c72',
          800: '#103e4c',
          900: '#081f26'
        }
      },
      spacing: {
        72: '18rem',
        84: '21rem',
        96: '24rem',
        128: '28rem',
        160: '36rem',
        256: '46rem'
      }
    }
  },
  variants: {},
  plugins: [
    require('@tailwindcss/custom-forms')
  ]
}
