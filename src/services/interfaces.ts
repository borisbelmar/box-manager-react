import { Box, Call, Patient, Report } from 'model'
import { CallRegister, ReportDates } from './contracts'

export interface BoxService {
  findAll: () => Promise<Box[]>
}

export interface CallService {
  findAll: () => Promise<Call[]>
  register: (callRegister: CallRegister) => Promise<void>
  remove: (callId: number) => Promise<void>
}

export interface PatientService {
  findAll: () => Promise<Patient[]>
}

export interface ReportService {
  getByDates: (reportDates: ReportDates) => Promise<Report>
}

export interface ServiceProvider {
  readonly boxService: BoxService
  readonly callService: CallService
  readonly patientService: PatientService
  readonly reportService: ReportService
}
