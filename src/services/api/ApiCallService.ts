import { CallRegister, ICall } from 'services/contracts'
import { CallService } from 'services/interfaces'
import { CallMapper } from 'services/mappers'
import ApiService from './ApiService'

export default class ApiCallService extends ApiService implements CallService {
  public findAll = async () => {
    const response = await this.axios.get<ICall[]>('/calls')
    return response.data.map(call => CallMapper.toClass(call))
  }

  public register = async ({ boxId, patientId }: CallRegister) => {
    await this.axios.post<ICall>('/calls', {
      boxId,
      patient: {
        id: patientId
      }
    })
  }

  public remove = async (callId: number) => {
    await this.axios.delete<void>(`/calls/${callId}`)
  }
}
