import { IPatient } from 'services/contracts'
import { PatientService } from 'services/interfaces'
import { PatientMapper } from 'services/mappers'
import ApiService from './ApiService'

export default class ApiPatientService extends ApiService implements PatientService {
  public findAll = async () => {
    const response = await this.axios.get<IPatient[]>('/patients')
    return response.data.map(patient => PatientMapper.toClass(patient))
  }
}
