import { IBox } from 'services/contracts'
import { BoxService } from 'services/interfaces'
import { BoxMapper } from 'services/mappers'
import ApiService from './ApiService'

export default class ApiBoxService extends ApiService implements BoxService {
  public findAll = async () => {
    const response = await this.axios.get<IBox[]>('/boxes/currentCall')
    return response.data.map(box => BoxMapper.toClass(box))
  }
}
