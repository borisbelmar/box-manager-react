import { IReport, ReportDates } from 'services/contracts'
import { ReportMapper } from 'services/mappers'
import { ReportService } from '../interfaces'
import ApiService from './ApiService'

export default class ApiReportService extends ApiService implements ReportService {
  public getByDates = async (reportDates: ReportDates) => {
    const response = await this.axios.get<IReport>('/report', { params: reportDates })
    return ReportMapper.toClass(response.data)
  }
}
