import axios, { AxiosInstance } from 'axios'

export default abstract class ApiService {
  protected axios: AxiosInstance

  constructor(baseURL: string) {
    this.axios = axios.create({
      baseURL
    })
  }
}
