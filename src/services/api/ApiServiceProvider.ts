import { BOX_MANAGER_URL, REPORTS_URL } from 'config'
import { ServiceProvider, BoxService, CallService, PatientService, ReportService } from 'services/interfaces'
import ApiBoxService from './ApiBoxService'
import ApiCallService from './ApiCallService'
import ApiPatientService from './ApiPatientService'
import ApiReportService from './ApiReportService'

export default class ApiServiceProvider implements ServiceProvider {
  readonly boxService: BoxService

  readonly callService: CallService

  readonly patientService: PatientService

  readonly reportService: ReportService

  constructor() {
    this.boxService = new ApiBoxService(BOX_MANAGER_URL)
    this.callService = new ApiCallService(BOX_MANAGER_URL)
    this.patientService = new ApiPatientService(BOX_MANAGER_URL)
    this.reportService = new ApiReportService(REPORTS_URL)
  }
}
