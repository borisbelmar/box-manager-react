import { Box } from 'model'
import { take, path } from 'ramda'
import { IBox, ICall } from 'services/contracts'
import CallMapper from './CallMapper'
import DoctorMapper from './DoctorMapper'

export default class BoxMapper {
  static toClass(plainBox: IBox): Box {
    const lastCall = path<ICall>(
      ['0'],
      take<ICall | undefined>(1, plainBox.calls as (ICall | undefined)[])
    )
    return new Box(
      DoctorMapper.toClass(plainBox.doctor),
      lastCall ? CallMapper.toClass(lastCall) : undefined
    ).withId(plainBox.id)
  }
}
