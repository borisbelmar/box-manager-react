import { Report } from 'model'
import { IReport } from 'services/contracts'

export default class ReportMaper {
  static toClass(plainReport: IReport): Report {
    return new Report(
      plainReport.headers,
      plainReport.data,
      plainReport.filename
    )
  }
}
