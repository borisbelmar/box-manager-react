import { Patient } from 'model'
import { IPatient } from 'services/contracts'

export default class PatientMapper {
  static toClass(plainPatient: IPatient): Patient {
    return new Patient(
      plainPatient.firstname,
      plainPatient.lastname,
      plainPatient.available
    ).withId(plainPatient.id)
  }
}
