import { Doctor } from 'model'
import { IPerson } from '../contracts'

export default class DoctorMapper {
  static toClass(plainDoctor: IPerson): Doctor {
    return new Doctor(
      plainDoctor.firstname,
      plainDoctor.lastname
    ).withId(plainDoctor.id)
  }
}
