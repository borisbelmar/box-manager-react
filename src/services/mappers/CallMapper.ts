import { Call } from 'model'
import { ICall } from '../contracts'
import PatientMapper from './PatientMapper'

export default class CallMapper {
  static toClass(plainCall: ICall): Call {
    return new Call(
      plainCall.boxId,
      PatientMapper.toClass(plainCall.patient),
      plainCall.expiration
    ).withId(plainCall.id)
  }
}
