interface IBase {
  id?: number
}

export interface IPerson extends IBase {
  firstname: string
  lastname: string
}

export interface IBox extends IBase {
  doctor: IPerson
  calls?: ICall[]
}

export interface ICall extends IBase {
  boxId: number
  patient: IPatient
  expiration: number
}

export interface IPatient extends IPerson {
  available: boolean
}

export interface CallRegister {
  boxId: number
  patientId: number
}

export interface ReportHeader {
  label: string
  key: string
}

export interface ReportData {
  doctor: string
  box: number
  date: string
  patients: string
}

export interface IReport {
  headers: ReportHeader[]
  data: ReportData[]
  filename: string
}

export interface ReportDates {
  from: string
  to: string
}
