import React, { createContext, ReactNode, useContext } from 'react'
import { ServiceProvider } from 'services/interfaces'

const ServiceContext = createContext<ServiceProvider>({} as ServiceProvider)

interface Props {
  children: ReactNode,
  serviceProvider: ServiceProvider
}

export const ServiceContextProvider = ({ children, serviceProvider }: Props) => {
  return (
    <ServiceContext.Provider value={serviceProvider}>
      {children}
    </ServiceContext.Provider>
  )
}

export const useServiceContext = () => useContext(ServiceContext)
