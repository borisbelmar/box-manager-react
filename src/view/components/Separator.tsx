import React from 'react'

const Separator = () => {
  return (
    <hr className="my-4 bg-opacity-25 bg-gray-700 h-px border-transparent" />
  )
}

export default Separator
