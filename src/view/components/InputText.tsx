import React from 'react'
import { path } from 'ramda'
import clsx from 'clsx'
import { DeepMap, FieldError } from 'react-hook-form'

type RefReturn =
  | string
  | ((instance: HTMLInputElement | null) => void)
  | React.RefObject<HTMLInputElement>
  | null
  | undefined

type TextInputProps<T> = {
  name: string
  label?: string
  type?: string
  className?: string
  placeholder?: string
  defaultValue?: string
  errors: DeepMap<T, FieldError>
  register: () => RefReturn
}

// eslint-disable-next-line @typescript-eslint/ban-types
const TextInput = (<T extends object>({
  name,
  label,
  type = 'text',
  errors,
  className,
  defaultValue,
  placeholder,
  register
}: TextInputProps<T>) => {
  const error = path<string>([name, 'message'], errors)
  return (
    <div className={clsx('mb-3', className)}>
      {label && <label className="block text-gray-700 mb-2" htmlFor={name}>{label}</label>}
      <div>
        <input
          className={clsx([
            'form-input mb-1 focus:outline-none focus:shadow-outline shadow-lg w-full',
            error ? 'border-red-500' : 'border-primary-500'
          ])}
          defaultValue={defaultValue}
          placeholder={placeholder}
          name={name}
          type={type}
          ref={register}
        />
      </div>
      {error && <small className="block text-red-500">{error}</small>}
    </div>
  )
})

export default TextInput
