import React from 'react'
import clsx from 'clsx'
import Spinner from './Spinner'

enum ButtonSize {
  sm = 'px-2 py-1 text-sm',
  md = 'px-4 py-2 text-base',
  lg = 'px-6 py-3 text-lg'
}

enum ButtonColor {
  primary = 'bg-primary-500 text-white hover:bg-primary-600 focus:bg-primary-700',
  disabled = 'cursor-default text-gray-500 bg-gray-300 hover:bg-gray-300 shadow-none hover:shadow-none focus:shadow-none',
  secondary = 'bg-primary-100 text-primary-500 hover:bg-primary-200 focus:bg-primary-300',
  danger = 'bg-red-500 text-white hover:bg-red-600 focus:bg-red-700'
}

interface ButtonProps extends React.ComponentPropsWithoutRef<'button'> {
  text: string
  isFullwidth?: boolean
  size?: 'sm' | 'md' | 'lg'
  color?: 'primary' | 'secondary' | 'danger'
  isLoading?: boolean
}

const Button = ({ text, onClick, type, disabled, isFullwidth, size = 'md', color = 'primary', className, isLoading }: ButtonProps) => {
  return (
    <button
      className={clsx([
        'flex-inline items-center justify-center rounded select-none transition duration-300 ease-in-out shadow-lg hover:shadow-md focus:shadow focus:outline-none',
        disabled ? ButtonColor.disabled : ButtonColor[color],
        isFullwidth && 'w-full',
        ButtonSize[size],
        className
      ])}
      onClick={onClick}
      type={type}
      disabled={disabled || isLoading}
    >
      {!isLoading ? text : <Spinner />}
    </button>
  )
}

export default Button
