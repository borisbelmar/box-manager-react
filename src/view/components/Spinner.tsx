import React from 'react'
import { motion } from 'framer-motion'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'

const animate = {
  scale: [0.75, 1, 0.75],
  opacity: [0.5, 1, 0.5],
  rotate: 360
}

const transition = {
  duration: 1,
  loop: Infinity
}

interface SpinnerProps {
  className?: string
}

const Spinner = ({ className }: SpinnerProps) => {
  return (
    <motion.div
      animate={animate}
      transition={transition}
      className={className}>
      <FontAwesomeIcon icon={faSpinner} />
    </motion.div>
  )
}

export default Spinner
