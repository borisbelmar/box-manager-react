import React from 'react'
import { Box } from 'model'

interface BoxCardProps {
  box: Box
}

const BoxCard = ({ box }: BoxCardProps) => {
  return box.call ? (
    <div className="flex bg-white justify-between items-stretch overflow-hidden rounded shadow-md">
      <div className="p-3 flex justify-center flex-col">
        <h5 className="text-sm text-gray-600">ID Paciente: {box.call?.patient.id}</h5>
        <h3 className="font-bold">{box.call?.patient.fullName}</h3>
        <p className="text-primary-500">{box.doctor?.doctorFullName}</p>
      </div>
      <div className="py-3 px-6 flex justify-center flex-col bg-primary-500 text-white text-center">
        <h4>BOX</h4>
        <h3 className="font-bold text-2xl">{box.id}</h3>
        <small>ID: {box.call?.id}</small>
      </div>
    </div>
  ) : (
    <div className="flex bg-white justify-between items-stretch overflow-hidden rounded shadow-md">
      <div className="p-3 flex justify-center flex-col">
        <h3 className="font-bold text-gray-500">Box en espera</h3>
        <p className="text-primary-500">{box.doctor?.doctorFullName}</p>
      </div>
      <div className="py-3 px-6 flex justify-center flex-col bg-gray-500 text-white text-center">
        <h4>BOX</h4>
        <h3 className="font-bold text-2xl">{box.id}</h3>
      </div>
    </div>
  )
}

export default BoxCard
