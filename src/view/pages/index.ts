import React from 'react'

export const BoxManager = React.lazy(() => import('./BoxManager'))
export const ReportPage = React.lazy(() => import('./ReportPage'))
export const NotFoundPage = React.lazy(() => import('./NotFoundPage'))
export { default as SuspenseLoading } from './SuspenseLoading'
