import React from 'react'
import { Link } from 'react-router-dom'
import Button from 'view/components/Button'

const NotFoundPage = () => {
  return (
    <div className="bg-gray-200 h-screen w-screen flex items-center justify-center">
      <div>
        <p className="text-3xl font-bold text-center mb-6">Error 404</p>
        <Link to="/">
          <Button text="Volver al manager" isFullwidth />
        </Link>
      </div>
    </div>
  )
}

export default React.memo(NotFoundPage)
