import React from 'react'
import { Report } from 'model'
import { CSVLink } from 'react-csv'
import { ReportDates } from 'services/contracts'

interface DataVisualizerProps {
  dates: ReportDates | undefined
  report: Report | undefined
}

const DataVisualizer = ({ dates, report }: DataVisualizerProps) => {
  return report?.data?.length ? (
    <div className="w-full">
      <h2 className="text-xl mb-1 font-semibold text-gray-800">Reporte de consultas desde {dates?.from} hasta {dates?.to} (Preview)</h2>
      <p className="mb-5 text-gray-600">Se omiten boxes sin pacientes atendidos</p>
      <CSVLink
        data={report.data}
        headers={report.headers}
        filename={report.filename}
        className="block mb-8 text-primary-500 hover:text-primary-700">
        Descargar CSV ({report.filename})
      </CSVLink>
      <table className="table-auto bg-gray-100 text-center w-full">
        <thead>
          <tr>
            {report.headers.map(header => (
              <th key={header.key} className="border px-4 py-2">{header.label}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {report.data.map((item, index) => (
            // eslint-disable-next-line react/no-array-index-key
            <tr key={index}>
              <td className="border px-4 py-2">{item.date}</td>
              <td className="border px-4 py-2">{item.box}</td>
              <td className="border px-4 py-2">{item.doctor}</td>
              <td className="border px-4 py-2">{item.patients}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  ) : (
    <div>
      {dates
        ? <p>No hay resultados para mostrar desde {dates?.from} hasta {dates?.to}</p>
        : <p>No hay registros para mostrar</p>}
    </div>
  )
}

export default React.memo(DataVisualizer)
