import React, { useState } from 'react'
import * as Yup from 'yup'
import { yupResolver } from '@hookform/resolvers'
import { useForm } from 'react-hook-form'
import { useQuery } from 'react-query'
import { ReportDates } from 'services/contracts'
import Button from 'view/components/Button'
import { useServiceContext } from 'view/context/ServiceContext'
import TextInput from 'view/components/InputText'
import { Link } from 'react-router-dom'
import Separator from 'view/components/Separator'
import Spinner from 'view/components/Spinner'
import DataVisualizer from './components/DataVisualizer'

const YYYYMMDD_REGEX = /^\d{4}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])$/

const ReportPage = () => {
  const [dates, setDates] = useState<ReportDates>()
  const { reportService } = useServiceContext()

  const { refetch, data, isLoading } = useQuery(
    [dates, 'report'],
    reportService.getByDates,
    {
      refetchOnWindowFocus: false,
      enabled: false
    }
  )

  const { register, errors, handleSubmit } = useForm<ReportDates>({
    resolver: yupResolver(Yup.object().shape({
      from: Yup.string()
        .test(
          'is-date',
          'No es una fecha válida',
          value => YYYYMMDD_REGEX.test(value || '')
        )
        .required('La fecha de inicio es requerida'),
      to: Yup.string()
        .test(
          'is-date',
          'No es una fecha válida',
          value => YYYYMMDD_REGEX.test(value || '')
        )
        .test({
          name: 'is-before',
          message: 'La fecha de término no debe ser mayor a la de inicio',
          test(value) {
            try {
              // eslint-disable-next-line react/no-this-in-sfc
              return new Date(this.parent.from) <= new Date(value)
            } catch {
              return false
            }
          }
        })
        .required('La fecha de término es requerida')
    }))
  })

  const onSubmit = handleSubmit(async (values: ReportDates) => {
    setDates(values)
    await refetch()
  })

  return (
    <div className="flex">
      <div className="p-8 w-128 h-screen sticky top-0 shadow-md flex-col flex justify-center">
        <h1 className="text-2xl font-bold text-center mb-8">Generar Reportes</h1>
        <form onSubmit={onSubmit}>
          <TextInput
            name="from"
            label="Desde"
            type="date"
            errors={errors}
            register={register} />
          <TextInput
            name="to"
            label="Hasta"
            type="date"
            errors={errors}
            register={register} />
          <Button text="Obtener reporte" isFullwidth type="submit" className="mt-3" />
        </form>
        <Separator />
        <Link to="/" className="block text-center text-primary-500">Volver al manager</Link>
      </div>
      <div className="p-8 w-full bg-gray-200 flex justify-center items-center">
        {isLoading ? (
          <div className="flex flex-col justify-center items-center">
            <Spinner className="text-3xl block text-primary-500" />
            <p className="ml-4">Estamos generando tu reporte</p>
          </div>
        ) : (
          <DataVisualizer report={data} dates={dates} />
        )}
      </div>
    </div>
  )
}

export default React.memo(ReportPage)
