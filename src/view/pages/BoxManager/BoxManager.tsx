import React, { useCallback, useEffect, useState } from 'react'
import { useServiceContext } from 'view/context/ServiceContext'
import { AxiosError } from 'axios'
import { useQuery } from 'react-query'
import { toast } from 'react-toastify'
import Manager from './Manager'
import BoxPresenter from './BoxPresenter'

function App() {
  const [showCall, setShowCall] = useState<boolean>(false)
  const [isRegistering, setRegistering] = useState<boolean>(false)

  const { boxService } = useServiceContext()
  const { data, refetch, isLoading } = useQuery('boxes', boxService.findAll, {
    onError: (e: AxiosError) => {
      console.error(e.response?.data)
      toast.error('Error al recuperar la información')
    },
    refetchInterval: 20200,
    refetchOnWindowFocus: false
  })

  const onRegister = useCallback(async () => {
    setRegistering(true)
    await refetch()
    setShowCall(true)
  }, [refetch])

  useEffect(() => {
    if (showCall) {
      const hideCallTimeout = setTimeout(() => {
        setRegistering(false)
        setShowCall(false)
      }, 8000)
      return () => {
        clearTimeout(hideCallTimeout)
      }
    }
  }, [showCall])

  return (
    <main className="flex">
      <Manager
        className="w-128 p-8 shadow-xl h-screen flex flex-col justify-center"
        onRegister={onRegister}
        onDeletion={refetch as () => Promise<void>}
        isRegistering={isRegistering} />
      <BoxPresenter
        showCall={showCall}
        boxData={data}
        isLoading={isLoading} />
    </main>
  )
}

export default App
