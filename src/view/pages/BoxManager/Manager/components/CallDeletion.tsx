import React from 'react'
import Button from 'view/components/Button'
import TextInput from 'view/components/InputText'
import { useServiceContext } from 'view/context/ServiceContext'
import { useForm } from 'react-hook-form'
import { useMutation } from 'react-query'
import { toast } from 'react-toastify'

interface CallDeletionProps {
  onDeletion: () => Promise<void>
}

interface ICallDeletion {
  callId: number
}

const CallDeletion = ({ onDeletion }: CallDeletionProps) => {
  const { register, errors, handleSubmit } = useForm<ICallDeletion>()
  const { callService } = useServiceContext()

  const [execDeletion, { isLoading }] = useMutation(callService.remove, {
    onError: () => {
      toast.error('La llamada que quieres eliminar no existe')
    },
    onSuccess: async () => {
      await onDeletion()
      toast.success('Llamada cancelada con éxito')
    }
  })

  const onSubmit = handleSubmit(async (values: ICallDeletion) => {
    await execDeletion(values.callId)
  })

  return (
    <form onSubmit={onSubmit}>
      <TextInput
        label="ID del llamado"
        name="callId"
        type="number"
        register={register}
        errors={errors} />
      <Button
        text="Cancelar llamado"
        color="danger"
        isLoading={isLoading}
        isFullwidth />
    </form>
  )
}

export default CallDeletion
