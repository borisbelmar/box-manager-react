import Button from 'view/components/Button'
import TextInput from 'view/components/InputText'
import React from 'react'
import { useForm } from 'react-hook-form'
import * as Yup from 'yup'
import { yupResolver } from '@hookform/resolvers'
import { useMutation, useQueryCache } from 'react-query'
import { useServiceContext } from 'view/context/ServiceContext'
import { AxiosError } from 'axios'
import { toast } from 'react-toastify'

interface CallRegisterProps {
  onRegister: () => void
  isRegistering: boolean
}

interface ICallRegister {
  boxId: number
  patientId: number
}

const CallRegister = ({ onRegister, isRegistering }: CallRegisterProps) => {
  const { register, errors, handleSubmit } = useForm<ICallRegister>({
    resolver: yupResolver(Yup.object().shape({
      boxId: Yup.number()
        .typeError('El id debe ser un número')
        .required('El id del box es requerido'),
      patientId: Yup.number()
        .typeError('El id debe ser un número')
        .required('El id del paciente es requerido')
    }))
  })
  const { callService } = useServiceContext()
  const cache = useQueryCache()

  const [execRegister, { isLoading }] = useMutation(callService.register, {
    onError: (e: AxiosError) => {
      console.error(e.response)
      toast.error('La llamada que intentas hacer ya existe')
    },
    onSuccess: () => {
      cache.invalidateQueries('boxes')
      onRegister()
    }
  })

  const onSubmit = handleSubmit(async (values: ICallRegister) => {
    await execRegister({
      boxId: values.boxId,
      patientId: values.patientId
    })
  })

  return (
    <form onSubmit={onSubmit}>
      <TextInput
        label="ID del box"
        name="boxId"
        type="number"
        register={register}
        errors={errors} />
      <TextInput
        label="ID del Paciente"
        name="patientId"
        type="number"
        register={register}
        errors={errors} />
      <Button
        text="Registrar llamado"
        isLoading={isRegistering || isLoading}
        isFullwidth />
    </form>
  )
}

export default React.memo(CallRegister)
