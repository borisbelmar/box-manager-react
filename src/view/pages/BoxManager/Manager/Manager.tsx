import React from 'react'
import { Link } from 'react-router-dom'
import Separator from 'view/components/Separator'
import Button from 'view/components/Button'
import CallDeletion from './components/CallDeletion'
import CallRegister from './components/CallRegister'

interface ManagerProps {
  className?: string
  onRegister: () => void
  onDeletion: () => Promise<void>
  isRegistering: boolean
}

const Manager = ({ className, onRegister, onDeletion, isRegistering }: ManagerProps) => {
  return (
    <div className={className}>
      <div className="mb-6">
        <h1 className="text-primary-500 font-bold text-xl mb-1">Súper Med</h1>
        <p className="text-gray-700 text-xs">Sistema de llamados para pacientes. Los pacientes van desde el ID 1 al 10, al igual que los boxes. La aplicación puede tardar en cargar la primera vez debido al hibernate de heroku.</p>
      </div>
      <CallRegister
        onRegister={onRegister}
        isRegistering={isRegistering} />
      <Separator />
      <CallDeletion
        onDeletion={onDeletion} />
      <Separator />
      <Link to="/report">
        <Button isFullwidth text="Descargar Reportes" />
      </Link>
      <Separator />
      <p className="text-gray-700 text-xs">Desarrollado por Boris Belmar, Juan Roco, Edgar González y Javier González.</p>
    </div>
  )
}

export default Manager
