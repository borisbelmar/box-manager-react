import React from 'react'
import { take } from 'ramda'
import { Box } from 'model'
import { Overlay } from 'view/layouts'
import { AnimatePresence, motion } from 'framer-motion'
import Spinner from 'view/components/Spinner'
import CallPresenter from './components/CallPresenter'
import BoxGrid from './components/BoxGrid'
import LastCallBoxes from './components/LastCallBoxes'

const motionOpacity = {
  opacity: [0, 2]
}

const motionTransition = {
  duration: 0.1
}

interface BoxPresenterProps {
  showCall: boolean
  boxData: Box[] | undefined
  isLoading: boolean
}

const BoxPresenter = ({ showCall, boxData, isLoading }: BoxPresenterProps) => {
  return !isLoading && boxData ? (
    <div className="relative w-full">
      <div className="flex flex-col w-full h-screen">
        <LastCallBoxes boxes={boxData} />
        <BoxGrid boxes={boxData} />
      </div>
      <AnimatePresence>
        {showCall && boxData && (
          <Overlay>
            <CallPresenter
              lastCallBoxes={
                take(3, boxData.filter(box => box.call))
              } />
          </Overlay>
        )}
      </AnimatePresence>
    </div>
  ) : (
    <motion.div
      className="flex flex-col w-full items-center justify-center h-screen"
      animate={motionOpacity}
      transition={motionTransition}>
      <Spinner className="text-5xl text-primary-500 mb-1" />
      <h3 className="text-xl mb-3">Estamos cargando los datos</h3>
      <p className="text-gray-600">Si es primera vez que lanzas la aplicación, puede tardar más de lo normal</p>
    </motion.div>
  )
}

export default BoxPresenter
