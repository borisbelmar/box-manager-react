import React from 'react'
import { path } from 'ramda'
import { Box } from 'model'
import BoxCard from 'view/components/BoxCard'

interface CallPresenterProps {
  lastCallBoxes: Box[]
}

const CallPresenter = ({ lastCallBoxes }: CallPresenterProps) => {
  const getBox = (index: number) => path<Box>([index], lastCallBoxes)
  return (
    <div className="w-256">
      {getBox(0) && (
        <div className="overflow-hidden rounded-lg bg-white mb-5">
          <header className="bg-primary-500 p-4 text-center">
            <h1 className="text-2xl font-bold text-white">Llamando a Box {lastCallBoxes[0].id}</h1>
          </header>
          <div className="flex flex-col justify-center items-center h-48">
            <h5 className="text-gray-700 mb-2">ID Paciente: {lastCallBoxes[0].call?.patient.id}</h5>
            <h3 className="text-3xl font-bold mb-2">{lastCallBoxes[0].call?.patient.fullName}</h3>
            <h4 className="text-xl font-bold text-primary-500">{lastCallBoxes[0].doctor.doctorFullName}</h4>
          </div>
        </div>
      )}
      {getBox(1) && (
        <div className="mb-5">
          <BoxCard box={lastCallBoxes[1]} />
        </div>
      )}
      {getBox(2) && (
        <BoxCard box={lastCallBoxes[2]} />
      )}
    </div>
  )
}

export default CallPresenter
