import React from 'react'
import BoxCard from 'view/components/BoxCard'
import { Box } from 'model'
import { motion } from 'framer-motion'

interface LastCallBoxesProps {
  boxes: Box[]
}

const LastCallBoxes = ({ boxes }: LastCallBoxesProps) => {
  return boxes.filter(box => box.call).length > 0 ? (
    <div className="px-6 py-12 bg-gray-100 w-full">
      <h1 className="text-2xl text-center mb-5 font-bold text-gray-700">Últimos llamados</h1>
      <div className="flex justify-center items-center my-auto">
        {boxes.map((box, index) => {
          return index < 3 && box.call ? (
            <motion.div
              key={box.id}
              className="p-2 w-1/3 self-center"
              animate={{ opacity: [0, 1], scale: [0.9, 1] }}>
              <BoxCard box={box} />
            </motion.div>
          ) : null
        })}
      </div>
    </div>
  ) : null
}

export default LastCallBoxes
