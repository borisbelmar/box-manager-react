import React, { useEffect, useState } from 'react'
import BoxCard from 'view/components/BoxCard'
import { Box } from 'model'
import { slice } from 'ramda'
import { motion } from 'framer-motion'

interface BoxGridProps {
  boxes: Box[]
}

const SLIDES_RANGES = [[0, 5], [5, 10]]

const getBoxesSlide = (boxes: Box[], slideIndex: number) => {
  return slice(SLIDES_RANGES[slideIndex][0], SLIDES_RANGES[slideIndex][1], boxes)
}

const BoxGrid = ({ boxes }: BoxGridProps) => {
  const [slideBoxes, setSlideBoxes] = useState<Box[]>([])

  useEffect(() => {
    const slides = SLIDES_RANGES.length
    setSlideBoxes(getBoxesSlide(boxes, 0))
    let slide = 1
    const sliderTimeout = setInterval(() => {
      setSlideBoxes(getBoxesSlide(boxes, slide))
      // eslint-disable-next-line no-plusplus
      slide++
      if (slide === slides) {
        slide = 0
      }
    }, 10000)

    return () => {
      clearInterval(sliderTimeout)
    }
  }, [boxes])

  return (
    <div className="bg-gray-200 w-full h-full p-6 flex flex-col justify-center items-center my-auto">
      {slideBoxes.map(box => (
        <motion.div
          key={box.id}
          className="p-2 w-full self-center"
          animate={{ opacity: [0, 1], scale: [0.9, 1] }}
          >
          <BoxCard box={box} />
        </motion.div>
      ))}
    </div>
  )
}

export default BoxGrid
