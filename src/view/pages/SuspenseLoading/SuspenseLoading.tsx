import React from 'react'
import { motion } from 'framer-motion'
import Spinner from 'view/components/Spinner'

const motionOpacity = {
  opacity: [0, 2]
}

const motionTransition = {
  duration: 0.1
}

const SuspenseLoading = () => (
  <motion.div animate={motionOpacity} transition={motionTransition}>
    <div className="flex flex-col w-full items-center justify-center h-screen">
      <Spinner className="text-5xl text-primary-500 mb-1" />
      <h3>Loading...</h3>
    </div>
  </motion.div>
)

export default SuspenseLoading
