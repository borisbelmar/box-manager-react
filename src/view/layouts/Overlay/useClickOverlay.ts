import { RefObject, useEffect } from 'react'

const useClickOverlay = (ref: RefObject<HTMLElement | undefined>, callback?: () => void) => {
  const handleClick = (e: Event) => {
    const node = e.target as HTMLElement
    if (ref.current && !ref.current.contains(node) && node?.id === 'overlay') {
      if (callback) {
        callback()
      }
    }
  }
  useEffect(() => {
    document.addEventListener('click', handleClick)
    return () => {
      document.removeEventListener('click', handleClick)
    }
  })
}

export default useClickOverlay
