export const BOX_MANAGER_URL = process.env.REACT_APP_BOX_MANAGER_API || 'http://localhost:6002'
export const REPORTS_URL = process.env.REACT_APP_REPORTS_API || 'http://localhost:6003'
