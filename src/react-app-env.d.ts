/// <reference types="react-scripts" />
declare namespace NodeJS {
  interface ProcessEnv {
    NODE_ENV: 'development' | 'production' | 'test'
    PUBLIC_URL: string
    REACT_APP_BOX_MANAGER_API: string
    REACT_APP_REPORTS_API: string
    REACT_APP_VERSION: string
  }
}
