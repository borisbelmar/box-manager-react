import AbstractPerson from './abstract/AbstractPerson'

export default class Patient extends AbstractPerson {
  private _available: boolean

  constructor(firstname: string, lastname: string, available: boolean) {
    super(firstname, lastname)
    this._available = available
  }

  public get available() {
    return this._available
  }
}
