import BaseEntity from './abstract/BaseEntity'
import Patient from './Patient'

export default class Call extends BaseEntity {
  private _boxId: number

  private _patient: Patient

  private _expiration: number

  constructor(boxId: number, patient: Patient, expiration: number) {
    super()
    this._boxId = boxId
    this._patient = patient
    this._expiration = expiration
  }

  public get boxId() {
    return this._boxId
  }

  public get patient() {
    return this._patient
  }

  public get expiration() {
    return this._expiration
  }
}
