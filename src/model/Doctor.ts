import AbstractPerson from './abstract/AbstractPerson'

export default class Doctor extends AbstractPerson {
  public get doctorFullName() {
    return `Dr. ${this._firstname} ${this._lastname}`
  }
}
