import { ReportData, ReportHeader } from '../services/contracts'

export default class Report {
  private _headers : ReportHeader[]

  private _data : ReportData[]

  private _filename : string

  constructor(headers: ReportHeader[], data: ReportData[], filename: string) {
    this._headers = headers
    this._data = data
    this._filename = filename
  }

  public get headers() {
    return this._headers
  }

  public get data() {
    return this._data
  }

  public get filename() {
    return this._filename
  }
}
