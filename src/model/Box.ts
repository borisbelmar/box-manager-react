import BaseEntity from './abstract/BaseEntity'
import Call from './Call'
import Doctor from './Doctor'

export default class Box extends BaseEntity {
  private _doctor: Doctor

  private _call?: Call

  constructor(doctor: Doctor, call?: Call) {
    super()
    this._doctor = doctor
    this._call = call
  }

  public get doctor() {
    return this._doctor
  }

  public get call() {
    return this._call
  }
}
