export default abstract class BaseEntity {
  private _id?: number

  public get id() {
    return this._id
  }

  public set id(id: number | undefined) {
    this._id = id
  }

  public withId(id: number | undefined) {
    this._id = id
    return this
  }
}
