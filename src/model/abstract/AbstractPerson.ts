import BaseEntity from './BaseEntity'

export default abstract class AbstractPerson extends BaseEntity {
  protected _firstname: string

  protected _lastname: string

  constructor(firstname: string, lastname: string) {
    super()
    this._firstname = firstname
    this._lastname = lastname
  }

  public get fullName() {
    return `${this._firstname} ${this._lastname}`
  }
}
