import React, { Suspense } from 'react'
import { ServiceContextProvider } from 'view/context/ServiceContext'
import { ToastContainer } from 'react-toastify'
import ApiServiceProvider from 'services/api/ApiServiceProvider'
import { BoxManager, ReportPage, SuspenseLoading, NotFoundPage } from 'view/pages'
import { ReactQueryCacheProvider } from 'react-query'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

function App() {
  return (
    <>
      <ReactQueryCacheProvider>
        <ServiceContextProvider serviceProvider={new ApiServiceProvider()}>
          <BrowserRouter>
            <Suspense fallback={<SuspenseLoading />}>
              <Switch>
                <Route exact path="/" component={BoxManager} />
                <Route exact path="/report" component={ReportPage} />
                <Route component={NotFoundPage} />
              </Switch>
            </Suspense>
          </BrowserRouter>
        </ServiceContextProvider>
      </ReactQueryCacheProvider>
      <ToastContainer
        position="bottom-right"
        limit={3}
        autoClose={3000}
      />
    </>
  )
}

export default App
